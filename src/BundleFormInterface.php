<?php

namespace Drupal\bundle_form;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for node_bundle_form plugins.
 */
interface BundleFormInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  /**
   * Override $form array.
   *
   * @param array $form
   *   Form to override.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Current node.
   */
  public function overrideForm(array &$form, FormStateInterface $form_state, EntityInterface $entity = NULL): void;

}
