<?php

namespace Drupal\bundle_form;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Render\Element\Form;

/**
 * NodeBundleForm plugin manager.
 */
class BundleFormPluginManager extends DefaultPluginManager {

  /**
   * Constructs NodeBundleFormPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/BundleForm',
      $namespaces,
      $module_handler,
      'Drupal\bundle_form\BundleFormInterface',
      'Drupal\bundle_form\Annotation\BundleForm'
    );
    $this->alterInfo('bundle_form_info');
    $this->setCacheBackend($cache_backend, 'bundle_form_plugins');
  }

  /**
   * {@inheritDoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    try {
      return $this->getFactory()->createInstance($plugin_id, $configuration);
    }
    catch (PluginNotFoundException) {
      return NULL;
    }
  }

  /**
   * Fetch plugin instances based on entity type and bundles.
   *
   * @param string $entityType
   *   Entity type to search.
   * @param string $bundle
   *   Bundle to search.
   *
   * @return \Drupal\bundle_form\BundleFormInterface[]
   *   List of plugin instances order on weight.
   */
  public function fetchInstances(string $entityType, string $bundle): array {
    $definitions = $this->getDefinitions();
    $definitions = array_filter($definitions, function (array $definition) use ($entityType, $bundle) {
      if (empty($definition['entity_type']) && empty($definition['bundle'])) {
        return FALSE;
      }
      if ($definition['entity_type'] === $entityType && $definition['bundle'] === $bundle) {
        return TRUE;
      }
      return FALSE;
    });

    usort($definitions, function (array $definitionA, array $definitionB) {
      return $definitionA['weight'] > $definitionB['weight'] ? 1 : -1;
    });
    return array_map(function (array $definition) {
      return $this->createInstance($definition['id']);
    }, $definitions);
  }
}
