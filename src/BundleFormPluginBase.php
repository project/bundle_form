<?php

namespace Drupal\bundle_form;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for node_bundle_form plugins.
 */
abstract class BundleFormPluginBase extends PluginBase implements BundleFormInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

}
