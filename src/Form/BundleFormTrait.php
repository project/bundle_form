<?php

namespace Drupal\bundle_form\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bundle_form\BundleFormPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define common treatment to apply bundle forms to entity type.
 */
trait BundleFormTrait {

  /**
   * NodeBundleFormPlugin manager.
   *
   * @var \Drupal\bundle_form\BundleFormPluginManager
   */
  protected BundleFormPluginManager $bundleFormPluginManager;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container): static {
    $instance = parent::create($container);
    $instance->setBundleFormPluginManager($container->get('plugin.manager.bundle_form'));
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);
    $plugins = $this->bundleFormPluginManager->fetchInstances($this->entity->getEntityTypeId(), $this->entity->bundle());
    foreach ($plugins as $plugin) {
      $plugin->overrideForm($form, $form_state, $this->entity);
    }
    return $form;
  }

  /**
   * Define nodeBundleFormPluginManager property.
   *
   * @param \Drupal\bundle_form\BundleFormPluginManager $bundleFormPluginManager
   *   NodeBundleFormPlugin manager.
   */
  public function setBundleFormPluginManager(BundleFormPluginManager $bundleFormPluginManager): void {
    $this->bundleFormPluginManager = $bundleFormPluginManager;
  }

}
