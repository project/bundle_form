<?php

namespace Drupal\bundle_form\Form;

use Drupal\taxonomy\TermForm as BaseTermForm;

/**
 * Override TermForm to dispatch between bundles class form.
 */
class TermForm extends BaseTermForm {

  use BundleFormTrait;

}
