<?php

namespace Drupal\bundle_form\Form;

use Drupal\node\NodeForm as BaseNodeForm;

/**
 * Override NodeForm to dispatch between bundles class form.
 */
class NodeForm extends BaseNodeForm {

  use BundleFormTrait;

}
