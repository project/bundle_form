<?php

namespace Drupal\bundle_form\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines node_bundle_form annotation object.
 *
 * @Annotation
 */
class BundleForm extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * Plugin entity concern to apply.
   *
   * @var string
   */
  public string $entity_type;

  /**
   * Plugin bundle to apply.
   *
   * @var string
   */
  public string $bundle;

  /**
   * Plugin weight to organise plugins.
   *
   * @var int
   */
  public int $weight;

}
