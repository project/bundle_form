<?php

namespace Drupal\bundle_form_examples\Plugin\BundleForm\Term;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bundle_form\Annotation\BundleForm;
use Drupal\bundle_form\BundleFormPluginBase;

/**
 * Implements override of tags term form bundle.
 *
 * @BundleForm(
 *   id = "bundle_form_examples_tags_form",
 *   entity_type = "taxonomy_term",
 *   bundle = "tags",
 *   weight = "10",
 *   label = @Translation("Tags term form override"),
 * )
 */
class TagsForm extends BundleFormPluginBase {

  /**
   * {@inheritDoc}
   */
  public function overrideForm(array &$form, FormStateInterface $form_state, EntityInterface $entity = NULL): void {
    $form['name']['#access'] = FALSE;
  }

}
