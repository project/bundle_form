<?php

namespace Drupal\bundle_form_examples\Plugin\BundleForm\Paragraph;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bundle_form\Annotation\BundleForm;
use Drupal\bundle_form\BundleFormPluginBase;

/**
 * Implements override of type 1 paragraph form bundle.
 *
 * @BundleForm(
 *   id = "bundle_form_examples_type_1_form",
 *   entity_type = "paragraph",
 *   bundle = "type_1",
 *   weight = "10",
 *   label = @Translation("Type 1 form override"),
 * )
 */
class Type1Form extends BundleFormPluginBase {

  /**
   * {@inheritDoc}
   */
  public function overrideForm(array &$form, FormStateInterface $form_state, EntityInterface $entity = NULL): void {
    $context = $form_state->get('context');
    $subform = &$form['subform'];
    $subform['field_test']['#access'] = FALSE;
  }

}
