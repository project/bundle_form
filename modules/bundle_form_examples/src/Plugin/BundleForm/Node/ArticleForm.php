<?php

namespace Drupal\bundle_form_examples\Plugin\BundleForm\Node;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bundle_form\Annotation\BundleForm;
use Drupal\bundle_form\BundleFormPluginBase;

/**
 * Implements override of article node form bundle.
 *
 * @BundleForm(
 *   id = "bundle_form_examples_article",
 *   entity_type = "node",
 *   bundle = "article",
 *   weight = "10",
 *   label = @Translation("Article node form override"),
 * )
 */
class ArticleForm extends BundleFormPluginBase {

  /**
   * {@inheritDoc}
   */
  public function overrideForm(array &$form, FormStateInterface $form_state, EntityInterface $entity = NULL): void {
    $form['title']['#access'] = FALSE;
  }

}
