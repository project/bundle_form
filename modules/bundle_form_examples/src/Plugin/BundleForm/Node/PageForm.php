<?php

namespace Drupal\bundle_form_examples\Plugin\BundleForm\Node;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\bundle_form\Annotation\BundleForm;
use Drupal\bundle_form\BundleFormPluginBase;

/**
 * Implements override of article node form bundle.
 *
 * @BundleForm(
 *   id = "bundle_form_examples_page",
 *   entity_type = "node",
 *   bundle = "page",
 *   weight = "10",
 *   label = @Translation("Article node form override"),
 * )
 */
class PageForm extends BundleFormPluginBase {

  use StringTranslationTrait;

  /**
   * {@inheritDoc}
   */
  public function overrideForm(array &$form, FormStateInterface $form_state, EntityInterface $entity = NULL): void {
    $form['body']['#access'] = FALSE;
  }

}
