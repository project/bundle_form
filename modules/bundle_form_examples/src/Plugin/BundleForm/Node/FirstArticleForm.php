<?php

namespace Drupal\bundle_form_examples\Plugin\BundleForm\Node;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bundle_form\Annotation\BundleForm;
use Drupal\bundle_form\BundleFormPluginBase;

/**
 * Implements override of article node form bundle.
 *
 * Like plugin weight is lower that bundle_form_examples_article plugin, it's executed first.
 *
 * @BundleForm(
 *   id = "bundle_form_examples_first_article",
 *   entity_type = "node",
 *   bundle = "article",
 *   weight = "-10",
 *   label = @Translation("Article node form override"),
 * )
 */
class FirstArticleForm extends BundleFormPluginBase {

  /**
   * {@inheritDoc}
   */
  public function overrideForm(array &$form, FormStateInterface $form_state, EntityInterface $entity = NULL): void {
    $form['title']['#access'] = TRUE;
  }

}
